#include "hwo.h"
#include "driver.h"

#define FAST 1.00
#define SLOW 0.60
#define MAX  6.1

cJSON   *driver(cJSON *json)
{
        cJSON   *response = NULL;

/*
        fprintf(stderr, "%i,", hwoMyCar->lap);
        fprintf(stderr, "%i\t", hwoMyCar->pieceIndex);
        fprintf(stderr, "%f\t", hwoMyCar->speed);
        fprintf(stderr, "%f\t", hwoMyCar->angle);
        fprintf(stderr, "%i\t", hwoMyCar->radius);
        fprintf(stderr, "\n");
*/
        switch(hwoMyCar->lap)
        {
        case -1:
        case 0:
                if (hwoMyCar->speed < 5.999)
                        response = throttle_msg(FAST);
                else
                        response = throttle_msg(0.6);
                break;
        case 1:
                if (hwoMyCar->speed < 6.499)
                        response = throttle_msg(FAST);
                else
                        response = throttle_msg(0.65);
                break;
        case 2:
                if (hwoMyCar->speed < 6.999)
                        response = throttle_msg(FAST);
                else
                        response = throttle_msg(0.7);
                break;
        default:
                response = throttle_msg(0.75);
        }

/*
        cJSON   *data;
        cJSON   *car;
        cJSON   *angle;

        data = cJSON_GetObjectItem(json, "data");
        car = cJSON_GetArrayItem(data, 0);
        angle = cJSON_GetObjectItem(car, "angle");
        //puts(cJSON_Print(angle));
        //puts(cJSON_Print(data));
        //puts("hello");
        //printf("angle: %f\n", angle->valuedouble);
        if (angle->valuedouble > MAX || angle->valuedouble < -MAX)
        else
                response = throttle_msg(FAST);

//        printf("phase %i\n", State->variables.phase);
        switch (State->variables.phase)
        {
        case 0:
        case 1:
        case 2:
        case 3:
                response = throttle_msg(1.0);
                State->variables.phase++;
                break;
        case 4:
                response = throttle_msg(0.0);
                if (hwoMyCar->speed < 0.0001)
                        State->variables.phase++;
                break;
        case 5:
                response = throttle_msg(0.1);
                if (hwoMyCar->speed > 0.9999)
                        State->variables.phase++;
                break;
        case 6:
                response = throttle_msg(0.0);
                if (hwoMyCar->speed < 0.0001)
                        State->variables.phase++;
                break;
        case 7:
                response = throttle_msg(0.2);
                if (hwoMyCar->speed > 1.9999)
                        State->variables.phase++;
                break;
        case 8:
                response = throttle_msg(0.1);
                if (hwoMyCar->speed < 1.0001)
                        State->variables.phase++;
                break;
        case 9:
                response = throttle_msg(0.2);
                if (hwoMyCar->speed > 1.9999)
                        State->variables.phase++;
                break;
        case 10:
                response = throttle_msg(0.0);
                if (hwoMyCar->speed < 0.0001)
                        State->variables.phase++;
                break;
        case 11:
                response = throttle_msg(0.5);
                if (hwoMyCar->speed > 4.9999)
                        State->variables.phase++;
                break;
        case 12:
                response = throttle_msg(0.1);
                if (hwoMyCar->speed < 1.0001)
                        State->variables.phase++;
                break;
        case 13:
                response = throttle_msg(0.5);
                if (hwoMyCar->speed > 4.9999)
                        State->variables.phase++;
                break;
        case 14:
                response = throttle_msg(0.0);
                if (hwoMyCar->speed < 0.0001)
                        exit(0);
                break;
        }
*/
        if (!response)
        {
                puts("ERROR: no answer");
                response = ping_msg();
        }
        return response;
}
