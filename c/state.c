#include <errno.h>
#include <netdb.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#include "cJSON.h"
#include "state.h"

state_t *State = NULL;

void    initState()
{
        State = malloc(sizeof(state_t));
        State->track            = NULL;
        State->graph            = NULL;
        State->competitors      = NULL;
        State->champion         = NULL;
        State->variables.phase = 0;
}

void    updateState(cJSON *json, char *msgType)
{
        char    *msg;

        puts(msgType);

        switch ( msgType[0] )
        {
        case 'c': // carPosition || crash
        case 's': // spawn
        case 'd': // dnf
                if (carEvent(json, msgType))
                        goto error;
                break;
        case 'g': // gameEnd || gameInit || gameStart
        case 't': // tournamentEnd
        case 'y': // yourCar
        case 'j': // join || joinRace
                if (raceInfo(json, msgType))
                        goto error;
                break;
        case 'f': // finish
        case 'l': // lapFinished
                if (gameInfo(json, msgType))
                        goto error;
                break;
        default:
        error:
                msg = cJSON_Print(json);
                printf("ERROR: '%s'.\n%s\n", msgType, msg);
                free(msg);
        }
}

int     carEvent(cJSON *json, char *msgType)
{
        if (!strcmp("carPositions", msgType))
        {
                updateAllCarsPosition(json);
                return 0;
        } else if (!strcmp("crash", msgType))
        {
                updateCarStatus(json, CRASHED);
                return 0;
        } else if (!strcmp("spawn", msgType))
        {
                updateCarStatus(json, ON_TRACK);
                return 0;
        } else if (!strcmp("dnf", msgType))
        {
                updateCarStatus(json, DNF);
                return 0;
        }
        return 1;
}

int     raceInfo(cJSON *json, char *msgType)
{
        if (!strcmp("gameInit", msgType))
        {       cJSON   *race, *session, *cars, *track;

                race = cJSON_GetObjectItem(json, "race");
                session = cJSON_GetObjectItem(race, "raceSession");
                cars = cJSON_GetObjectItem(race, "cars");
                track =  cJSON_GetObjectItem(race, "track");
                initTrack(track, session);
                initCompetitors(cars);
                return 0;
        } else if (!strcmp("gameStart", msgType))
        {
                puts("Race started.");
                allCarsOnTrack();
                return 0;
        } else if (!strcmp("gameEnd", msgType))
        {
//TODO
                puts("gameEnd");
                return 0;
        } else if (!strcmp("tournamentEnd", msgType))
        {
//TODO
                puts("tournamentEnd");
                return 0;
        } else if (!strcmp("yourCar", msgType))
        {
                cJSON   *name, *color;

                name = cJSON_GetObjectItem(json, "name");
                color = cJSON_GetObjectItem(json, "color");
                printf("%s's %s car will prevail.\n",
                        name->valuestring, color->valuestring);
                initChampion(name->valuestring, color->valuestring);
                return 0;
        } else if (!strcmp("join", msgType))
        {
                cJSON   *name, *key;

                name = cJSON_GetObjectItem(json, "name");
                key = cJSON_GetObjectItem(json, "key");
                printf("Joining game as '%s' with key '%s'.\n", name->valuestring, key->valuestring);
                return 0;
        } else if (!strcmp("joinRace", msgType))
        {
                cJSON   *botId, *name, *key;

                botId = cJSON_GetObjectItem(json, "botId");
                name = cJSON_GetObjectItem(botId, "name");
                key = cJSON_GetObjectItem(botId, "key");
                printf("Joining game as '%s' with key '%s'.\n", name->valuestring, key->valuestring);
                return 0;
        }
        return 1;
}

int     gameInfo(cJSON *json, char *msgType)
{
        if (!strcmp("finish", msgType))
        {
//TODO
                puts("finish");
                return 0;
        } else if (!strcmp("lapFinished", msgType))
        {
//TODO
                puts("lapFinished");
                return 0;
        }
        return 1;
}

