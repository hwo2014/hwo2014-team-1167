#include <errno.h>
#include <netdb.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#include "cJSON.h"
#include "state.h"
#include "rail.h"

competitor_t    *newCompetitor()
{
        competitor_t    *competitor;
        car_variables_t *variables;

        competitor = malloc(sizeof(competitor_t));
        variables = malloc(sizeof(car_variables_t));

        variables->carStatus            = READY;
        variables->speed                = -1;
        variables->angle                = 0;
        variables->piece                = NULL;
        variables->lane                 = NULL;
        variables->path                 = NULL;
        variables->pieceIndex           = -1;
        variables->inPieceDistance      = -1;
        variables->startLaneIndex       = -1;
        variables->endLaneIndex         = -1;
        variables->lap                  = -1;
        variables->prev                 = NULL;
        competitor->name                = NULL;
        competitor->color               = NULL;
        competitor->variables           = malloc(sizeof(car_variables_t*));
        competitor->variables[0]        = variables;
        return (competitor);
}

void    setCompetitorID(competitor_t *competitor, char *name, char *color)
{
        competitor->color = strdup(color);
        competitor->name = strdup(name);
}

void    initChampion(char *name, char *color)
{
        competitor_t    *champion;
        champion = newCompetitor();
        setCompetitorID(champion, name, color);

        State->champion = champion;
}

void    initCompetitors(cJSON *cars)
{
        int     n;

        n = cJSON_GetArraySize(cars);
        State->competitors = malloc((n + 1) * sizeof(competitor_t*));
        State->competitors[n] = NULL;
        puts("Preparing competitors:");
        for (; n > 0; n--)
        {
                cJSON   *car, *id, *name, *color;


                car = cJSON_GetArrayItem(cars, n - 1);
                id = cJSON_GetObjectItem(car, "id");
                name = cJSON_GetObjectItem(id, "name");
                color = cJSON_GetObjectItem(id, "color");
                if (!strcmp(color->valuestring, State->champion->color))
                {
                        cJSON   *dimensions, *width, *length, *guide;

                        dimensions = cJSON_GetObjectItem(car, "dimensions");
                        width = cJSON_GetObjectItem(dimensions, "width");
                        length = cJSON_GetObjectItem(dimensions, "length");
                        guide = cJSON_GetObjectItem(dimensions, "guideFlagPosition");
                        State->variables.carWidth = width->valueint;
                        State->variables.carLength = length->valueint;
                        State->variables.carGuide = guide->valueint;
                        State->competitors[n - 1] = State->champion;
                } else {
                        competitor_t    *competitor;

                        competitor = newCompetitor();
                        setCompetitorID(competitor, name->valuestring, color->valuestring);
                        State->competitors[n - 1] = competitor;
                }
                printf("\t%s (%s car)\n", name->valuestring, color->valuestring);
        }
        printf("Cars are %i units long by %i units whide.\nGuide flag located %i units from the front.\n",
                State->variables.carLength, State->variables.carWidth, State->variables.carGuide);
}

competitor_t    *getCar(char *color)
{
        int             i;

        for (i = 0; State->competitors[i]; i++)
                if (! strcmp(State->competitors[i]->color, color))
                        break;
        return (State->competitors[i]);
}

void    allCarsOnTrack()
{
        int             i;

        for (i = 0; State->competitors[i]; i++)
                State->competitors[i]->variables[0]->carStatus = ON_TRACK;
}

void    updateAllCarsPosition(cJSON *data)
{
        int             n, i;
        cJSON           *car;
        competitor_t    *competitor;

        n = cJSON_GetArraySize(data);
        for (i = 0; i < n; i++)
        {
                cJSON   *id, *color, *angle, *position, *pieceIndex; 
                cJSON   *distance, *lap, *lanes, *startLane, *endLane;

                car = cJSON_GetArrayItem(data, i);
                id              = cJSON_GetObjectItem(car, "id");
                angle           = cJSON_GetObjectItem(car, "angle");
                position        = cJSON_GetObjectItem(car, "piecePosition");
                color           = cJSON_GetObjectItem(id, "color");
                pieceIndex      = cJSON_GetObjectItem(position, "pieceIndex");
                distance        = cJSON_GetObjectItem(position, "inPieceDistance");
                lap             = cJSON_GetObjectItem(position, "lap");
                lanes           = cJSON_GetObjectItem(position, "lane");
                startLane       = cJSON_GetObjectItem(lanes, "startLaneIndex");
                endLane         = cJSON_GetObjectItem(lanes, "endLaneIndex");
                competitor = getCar(color->valuestring);
                updateCarPosition(competitor,
                                  angle->valuedouble, pieceIndex->valueint,
                                  distance->valuedouble, startLane->valueint,
                                  endLane->valueint, lap->valueint);
        }
}

void    updateCarPosition(competitor_t *car, double angle, int pieceIndex,
                          double distance, int startLane, int endLane, int lap)
{
        car_variables_t         *updated;

        updated = malloc(sizeof(car_variables_t));
        updated->carStatus = car->variables[0]->carStatus;
        updated->angle = angle;
        updated->lane = State->track->lanes[startLane];
//TODO  updated->path
        updated->radius = railRadius(pieceIndex, startLane, endLane);
        updated->pieceIndex = pieceIndex;
        updated->inPieceDistance = distance;
        updated->startLaneIndex = startLane;
        updated->endLaneIndex = endLane;
        updated->lap = lap;
        updated->prev = car->variables[0];
        if (car->variables[0]->pieceIndex != -1)
                if (car->variables[0]->pieceIndex != updated->pieceIndex)
                        updated->speed = updated->inPieceDistance
                                - car->variables[0]->inPieceDistance
                                        + railLength(car->variables[0]->pieceIndex,
                                                car->variables[0]->startLaneIndex,
                                                car->variables[0]->endLaneIndex);
                else
                        updated->speed = updated->inPieceDistance
                                - car->variables[0]->inPieceDistance;
        else
                updated->speed = distance;
        car->variables[0] = updated;
}

void    updateCarStatus(cJSON *data, car_status_t status)
{
        cJSON           *info;
        competitor_t    *car;

        switch (status)
        {
        case CRASHED:
        case ON_TRACK:
                info = cJSON_GetObjectItem(data, "color");
                break;
        case DNF:
                info = cJSON_GetObjectItem(cJSON_GetObjectItem(data, "car"), "color");
                break;
        case READY: // gcc complains if not all cases are handled.
        case FINISHED:
                break;
        }
        car = getCar(info->valuestring);
        car->variables[0]->carStatus = status;
        switch (status)
        {
        case CRASHED:
                printf("%s (%s car) crashed.\n", car->name, car->color);
                break;
        case ON_TRACK:
                printf("%s (%s car) is back on track.\n", car->name, car->color);
                break;
        case DNF:
                info = cJSON_GetObjectItem(data, "reason");
                printf("%s (%s car) was disqualified. Reason: %s\n",
                        car->name, car->color, info->valuestring);
                break;
        case READY:
        case FINISHED:
                break;
        }
}
