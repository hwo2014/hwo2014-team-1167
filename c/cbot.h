
cJSON *ping_msg();
cJSON   *join_msg(int ac, char **av);
cJSON *throttle_msg(double throttle);
cJSON *make_msg(char *type, cJSON *msg);

cJSON *read_msg(int fd);
void write_msg(int fd, cJSON *msg);

void error(char *fmt, ...);
int connect_to(char *hostname, char *port);
void log_message(char *msg_type_name, cJSON *msg);
