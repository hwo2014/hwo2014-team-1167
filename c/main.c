#include <errno.h>
#include <netdb.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#include "cJSON.h"
#include "cbot.h"
#include "driver.h"
#include "state.h"

int main(int argc, char *argv[])
{
        int sock;
        cJSON *json;
        int     i;
        
        puts("still alive");
        for (i = 0; i < argc; i++)
        {
                puts(argv[i]);
        }


        if (argc < 5 || argc > 8)
                error("Usage: bot host port botname botkey [trackname] [carcount] [password]\n");

        sock = connect_to(argv[1], argv[2]);

        puts("opening sock");

        json = join_msg(argc, argv);
        write_msg(sock, json);
        cJSON_Delete(json);

        puts("initState");

        initState();

        while ((json = read_msg(sock)) != NULL) {
                cJSON *msg, *msg_type, *data;
                char *msg_type_name;

                puts(cJSON_PrintUnformatted(json));
                msg_type = cJSON_GetObjectItem(json, "msgType");
                if (msg_type == NULL)
                        error("missing msgType field");
                msg_type_name = msg_type->valuestring;
//                if (!strcmp("carPositions", msg_type_name)) {
//                        msg = driver(json);
//        exit(0);
//                } else {
                        msg = ping_msg();
//                }
                write_msg(sock, msg);
                data = cJSON_GetObjectItem(json, "data");
                updateState(data, msg_type_name);

                cJSON_Delete(msg);
                cJSON_Delete(json);
    }

    return 0;
}
