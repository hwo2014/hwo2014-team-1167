#include <errno.h>
#include <netdb.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <math.h>
#include "cJSON.h"
#include "rail.h"
#include "state.h"

int     railRadius(int piece, int startLane, int endLane)
{
        int     r;

//TODO: gerer les changement de voie
        if(!State->track->pieces[piece]->isCurved)
                return 0;
        if (State->track->pieces[piece]->angle > 0)
                r = State->track->pieces[piece]->radius
                        - State->track->lanes[startLane]->distanceFromCenter;
        else
                r = State->track->pieces[piece]->radius
                        + State->track->lanes[startLane]->distanceFromCenter;
        if (State->track->pieces[piece]->angle > 0)
                return (r);
        else
                return (-r);
}

double  railLength(int piece, int startLane, int endLane)
{
        int     r;
        double  a;

//TODO: gerer les changement de voie
        if(!State->track->pieces[piece]->isCurved)
                return (State->track->pieces[piece]->length);
        r = railRadius(piece, startLane, endLane);
        a = State->track->pieces[piece]->angle * 0.0174532925;
        return (a*r);
}
