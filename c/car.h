typedef enum {
        READY,
        ON_TRACK,
        CRASHED,
        DNF,
        FINISHED,
} car_status_t;

struct car_variables_s {
        car_status_t            carStatus;
        double                  speed;
        double                  angle;
        piece_t                 *piece;
        int                     radius;
        lane_t                  *lane;
        path_t                  *path;
        int                     pieceIndex;
        double                  inPieceDistance;
        int                     startLaneIndex;
        int                     endLaneIndex;
        int                     lap;
        struct car_variables_s  *prev;
};

typedef struct car_variables_s car_variables_t;

typedef struct {
        char                    *name;
        char                    *color;
        car_variables_t         **variables;
} competitor_t;

competitor_t                    *newCompetitor();
void                            initChampion(char *name, char *color);
void                            setCarID(competitor_t *competitor, char *name, char *color);
void                            initCompetitors(cJSON *cars);
void                            updateCarStatus(cJSON *data, car_status_t status);
competitor_t                    *getCar(char *color);
void                            allCarsOnTrack();
void                            updateAllCarsPosition(cJSON *data);
void                            updateCarPosition(competitor_t *car, double angle, int pieceIndex, double distance, int startLane, int endLane, int lap);
