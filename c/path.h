typedef enum {
        LEFT,
        STRAIGHT,
        RIGHT
} steer_t;

struct path_s {
        lane_t                  *lane;
        piece_t                 *piece;
        int                     length;
        int                     radius;
        steer_t                 best;
        int                     DistanceToFinishLine[3];
        struct path_s           *next;
};

typedef struct path_s           path_t;

