#include "track.h"
#include "path.h"
#include "car.h"

typedef struct {
        int     carLength;
        int     carWidth;
        int     carGuide;
        int     phase;
} variables_t;

typedef struct {
        track_t                 *track;
        path_t                  *graph;
        competitor_t            **competitors;
        competitor_t            *champion;
        variables_t             variables;
} state_t;

extern state_t                  *State;

void                            initState();
void                            updateState(cJSON *json, char *msgType);
int                             carEvent(cJSON *json, char *msgType);
int                             raceInfo(cJSON *json, char *msgType);
int                             gameInfo(cJSON *json, char *msgType);

