#include <errno.h>
#include <netdb.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <math.h>

#include "cJSON.h"
#include "state.h"

track_t *newTrack(char *id, char *name, int laps, int maxLapTimeMs, int quickRace)
{
        track_t *track;

        track = malloc(sizeof(track_t));
        track->id               = strdup(id);
        track->name             = strdup(name);
        track->pieces            = NULL;
        track->lanes            = NULL;
        track->laps             = laps;
        track->maxLapTimeMs     = maxLapTimeMs;
        track->quickRace        = quickRace;
        return (track);
}

piece_t *newStraightPiece(double length, int hasSwitch)
{
        piece_t *piece;

        piece = malloc(sizeof(piece_t));
        piece->isCurved = 0;
        piece->hasSwitch = hasSwitch;
        piece->length = length;
        piece->angle = 0;
        piece->radius = 0;
        return (piece);
}

piece_t *newCurvedPiece(int radius, double angle, int hasSwitch)
{
        piece_t *piece;

        piece = malloc(sizeof(piece_t));
        piece->isCurved = 1;
        piece->hasSwitch = hasSwitch;
        piece->angle = angle;
        piece->radius = radius;
        piece->length = fabs(angle) * radius *  0.0174532925;
        return (piece);
}

lane_t  *newLane(int index, int distanceFromCenter)
{
        lane_t  *lane;

        lane = malloc(sizeof(lane_t));
        lane->index = index;
        lane->distanceFromCenter = distanceFromCenter;
        return (lane);
}

void    initTrack(cJSON *race, cJSON *session)
{
        cJSON   *id, *name, *pieces, *lanes, *laps, *maxLapTimeMs, *quickRace;
        char    *raceType;
        int     i, n;

        id              = cJSON_GetObjectItem(race, "id");
        name            = cJSON_GetObjectItem(race, "name");
        pieces           = cJSON_GetObjectItem(race, "pieces");
        lanes           = cJSON_GetObjectItem(race, "lanes");
        laps            = cJSON_GetObjectItem(session, "laps");
        maxLapTimeMs    = cJSON_GetObjectItem(session, "maxLapTimeMs");
        quickRace       = cJSON_GetObjectItem(session, "quickRace");
        raceType = (quickRace->valueint)?("quick race"):("race");
        if (laps)
                printf("Geting ready for a %i lap %s on %s. Laps must not exceed %i ms.\nTrack id: %s\n",
                        laps->valueint, raceType, name->valuestring, maxLapTimeMs->valueint, id->valuestring);
        else 
        {
                puts(cJSON_Print(session));
                exit(0);
        }
        State->track = newTrack(id->valuestring, name->valuestring,
                                laps->valueint, maxLapTimeMs->valueint, quickRace->valueint);
        n = cJSON_GetArraySize(pieces);
        State->track->pieces = malloc((n + 1) * sizeof(piece_t*));
        State->track->pieces[n] = NULL;
        for (i = 0; i < n; i++)
        {
                cJSON   *piece, *length, *hasSwitch;

                piece = cJSON_GetArrayItem(pieces, i);
                length = cJSON_GetObjectItem(piece, "length");
                hasSwitch = cJSON_GetObjectItem(piece, "switch");
                if (length)
                        State->track->pieces[i] = newStraightPiece(length->valuedouble,
                                                        (hasSwitch)?(hasSwitch->valueint):(0));
                else {
                        cJSON   *radius, *angle;

                        radius = cJSON_GetObjectItem(piece, "radius");
                        angle = cJSON_GetObjectItem(piece, "angle");
                        State->track->pieces[i] = newCurvedPiece(radius->valueint, angle->valuedouble,
                                                        (hasSwitch)?(hasSwitch->valueint):(0));
                }
        }
        n = cJSON_GetArraySize(lanes);
        State->track->lanes = malloc((n + 1) * sizeof(lane_t*));
        State->track->lanes[n] = NULL;
        for (i = 0; i < n; i++)
        {
                cJSON   *lane, *index, *distanceFromCenter;

                lane = cJSON_GetArrayItem(lanes, i);
                index = cJSON_GetObjectItem(lane, "index");
                distanceFromCenter = cJSON_GetObjectItem(lane, "distanceFromCenter");
                State->track->lanes[index->valueint] = newLane(index->valueint, distanceFromCenter->valueint);
        }
}
