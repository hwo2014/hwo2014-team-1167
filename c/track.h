typedef struct {
        int                     index;
        int                     distanceFromCenter;
} lane_t;

typedef struct {
        int                     isCurved;
        int                     hasSwitch;
        double                  length;
        double                  angle;
        int                     radius;
} piece_t;

typedef struct {
        char                    *id;
        char                    *name;
        piece_t                 **pieces;
        lane_t                  **lanes;
        int                     laps;
        int                     maxLapTimeMs;
        int                     quickRace;
} track_t;

void                            initTrack(cJSON *race, cJSON *session);
track_t                         *newTrack(char *id, char *name, int laps, int maxLapTimeMs, int quickRace);
piece_t                         *newStraightPiece(double length, int hasSwitch);
piece_t                         *newCurvedPiece(int radius, double angle, int hasSwitch);
lane_t                          *newLane(int index, int distanceFromCenter);
